#version 120

uniform samplerCube skybox_texture;

varying vec3 frag_cube_uv;

void main () {
	gl_FragColor = textureCube(skybox_texture, frag_cube_uv);
}
