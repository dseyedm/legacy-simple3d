#version 120
uniform sampler2D diffuse_texture;
varying vec2 custom_uv_frag;
void main() {
	gl_FragColor = texture2D(diffuse_texture, custom_uv_frag);
}
