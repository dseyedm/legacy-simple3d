#version 120

attribute vec3 s3d_geometry_vertex;
attribute vec2 s3d_geometry_uv;

varying vec2 custom_uv_frag;
void main() {
	custom_uv_frag = s3d_geometry_uv;
    gl_Position = vec4(s3d_geometry_vertex, 1.0);
}
