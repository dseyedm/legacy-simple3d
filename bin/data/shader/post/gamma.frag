#version 120
uniform sampler2D frame_texture;
uniform float gamma;
varying vec2 custom_uv_frag;
void main(void) {
    vec3 color = texture2D(frame_texture, custom_uv_frag).rgb;
    gl_FragColor.rgb = pow(color, vec3(1.0 / gamma));
    gl_FragColor.a = 1.0;
}
