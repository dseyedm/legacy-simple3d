#version 120
uniform sampler2D frame_texture;
varying vec2 custom_uv_frag;
void main() {
	vec4 color = texture2D(frame_texture, custom_uv_frag);
	float average = (color.r + color.b + color.g) / 3;
	gl_FragColor = vec4(average, average, average, 1.0);
}
