#version 120
uniform sampler2D frame_texture;
varying vec2 custom_uv_frag;
void main() {
    vec4 sum = vec4(0);
    vec2 texcoord = custom_uv_frag;
    int j;
    int i;
	
	if(custom_uv_frag.x < 0.5) {
		for( i= -4 ; i < 4; i++) {
			for (j = -3; j < 3; j++) {
				sum += texture2D(frame_texture, texcoord + vec2(j, i)*0.004) * 0.25;
			}
		}
		if (texture2D(frame_texture, texcoord).r < 0.3) {
			gl_FragColor = sum*sum*0.012 + texture2D(frame_texture, texcoord);
		} else {
			if (texture2D(frame_texture, texcoord).r < 0.5) {
				gl_FragColor = sum*sum*0.009 + texture2D(frame_texture, texcoord);
			} else {
				gl_FragColor = sum*sum*0.0075 + texture2D(frame_texture, texcoord);
			}
		}
	} else {
		gl_FragColor = texture2D(frame_texture, texcoord);
	}
}
