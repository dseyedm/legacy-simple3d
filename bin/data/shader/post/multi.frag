#version 120
uniform sampler2D frame_texture;
varying vec2 custom_uv_frag;
void main() {
	gl_FragColor = texture2D(frame_texture, custom_uv_frag * 4);
}
