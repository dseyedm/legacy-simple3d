#version 120
uniform mat4 s3d_camera_proj;
uniform mat4 s3d_camera_view;
uniform mat4 s3d_model_transform;

attribute vec3 s3d_geometry_vertex;
attribute vec2 s3d_geometry_uv;

varying vec2 custom_uv_frag;
void main() {
	custom_uv_frag = s3d_geometry_uv;
    gl_Position = s3d_camera_proj * s3d_camera_view * s3d_model_transform * vec4(s3d_geometry_vertex, 1.0);
}
