#include "s3d/scene.h"
#include "s3d/model.h"

#include <cstdlib>
#include <time.h>
#include <iostream>

s3d::Window gWindow;

int context() {
    try {
        srand(time(nullptr));

        // Window
        sf::ContextSettings settings;
        settings.depthBits          = 24;
        settings.stencilBits        = 8;
        settings.antialiasingLevel  = 8;
        settings.majorVersion       = 2;
        settings.minorVersion       = 1;
        gWindow.get()->create(sf::VideoMode(800, 600), "Simple3D", sf::Style::Close, settings);
        gWindow.initGLEW();

        // Upload the default geometry
        s3d::Geometry::Upload();

        // Geometry
        s3d::Geometry monkeyGeometry;
        monkeyGeometry.loadObj("data/mesh/monkey.obj");
        monkeyGeometry.upload();

        s3d::Geometry knifeGeometry;
        knifeGeometry.loadObj("data/mesh/knife.obj");
        knifeGeometry.upload();

        // Textures
        s3d::Texture2d dirtTexture;
        dirtTexture.loadFromFile("data/texture/dirt.png");
        dirtTexture.setFiltering(s3d::Texture::Filtering::Nearest);

        s3d::Texture3d tronCubemap;
        tronCubemap.loadFromFiles("data/texture/sky/xp.jpg", "data/texture/sky/xn.jpg",
                                  "data/texture/sky/yp.jpg", "data/texture/sky/yn.jpg",
                                  "data/texture/sky/zp.jpg", "data/texture/sky/zn.jpg");

        // Shaders
        s3d::Shader skyboxShader;
        skyboxShader.loadFromFile("data/shader/skybox.vert", "data/shader/skybox.frag");

        s3d::Shader diffuseShader;
        diffuseShader.loadFromFile("data/shader/static_model_diffuse.vert", "data/shader/static_model_diffuse.frag");

        s3d::Shader fxaaShader;
        fxaaShader.loadFromFile("data/shader/post/frame.vert", "data/shader/post/fxaa.frag");

        s3d::Shader gammaShader;
        gammaShader.loadFromFile("data/shader/post/frame.vert", "data/shader/post/gamma.frag");

        // Materials
        s3d::Material skyboxMaterial;
        skyboxMaterial.setShader(&skyboxShader);
        skyboxMaterial.addTexture(&tronCubemap, "skybox_texture");

        s3d::Material monkeyMaterial;
        monkeyMaterial.setShader(&diffuseShader);
        monkeyMaterial.addTexture(&dirtTexture, "diffuse_texture");

        s3d::Material knifeMaterial;
        knifeMaterial.setShader(&diffuseShader);
        knifeMaterial.addTexture(&dirtTexture, "diffuse_texture");

        // Models
        s3d::StaticModel monkey;
        monkey.setGeometry(&monkeyGeometry);
        monkey.setMaterial(&monkeyMaterial);

        s3d::StaticModel knife;
        knife.setGeometry(&knifeGeometry);
        knife.setMaterial(&knifeMaterial);
        knife.setDepthClear(true);

        // Skybox
        s3d::StaticModel tronSkybox;
        tronSkybox.setGeometry(&s3d::Geometry::InverseCube);
        tronSkybox.setMaterial(&skyboxMaterial);
        tronSkybox.setDepthMask(false);

        // Camera
        s3d::Camera camera;
        camera.setClearColor(glm::vec3(0.3, 0.3, 0.3));
        camera.setFieldOfView(90);
        camera.setAspectRatio((float)gWindow.get()->getSize().x / (float)gWindow.get()->getSize().y);
        camera.setNearFar(0.1, 1000);
        camera.transform()->setPosition(glm::vec3(2, 1, 1));
        camera.transform()->face(glm::vec3(0));

        // Scene
        s3d::Scene scene;
        scene.setCamera(&camera);
        scene.addModel(&tronSkybox);
        scene.addModel(&monkey);
        scene.addModel(&knife);

        // SFML
        sf::Font font;
        if(!font.loadFromFile("data/font/font.ttf")) {
            throw std::runtime_error("failed to load font");
        }
        sf::Text tfps;
        tfps.setFont(font);
        tfps.setCharacterSize(22);
        tfps.setString("fps: 60");

        sf::Clock dtTimer;
        sf::Clock clock;
        s3d::Input input;
        bool running = true;
        while(running && gWindow.get()->isOpen()) {
            s3d::Window::checkGLErrors(&gWindow);

            sf::Event event;
            while(gWindow.get()->pollEvent(event)) {
                if(event.type == sf::Event::LostFocus) {
                    s3d::Input::setGlobalEnabled(false);
                } else if(event.type == sf::Event::GainedFocus) {
                    s3d::Input::setGlobalEnabled(true);
                } else if(event.type == sf::Event::Resized) {
                    glViewport(0, 0, event.size.width, event.size.height);
                    gWindow.get()->setSize(sf::Vector2u(event.size.width, event.size.height));
                    camera.setAspectRatio((float)event.size.width / (float)event.size.height);
                } else if(event.type == sf::Event::Closed) {
                    running = false;
                }
            }
            if(gWindow.get()->isOpen()) {
                const float dt = dtTimer.restart().asSeconds();
                const float fps = 1 / dt;
                tfps.setString("fps: " + s3d::util::toString((int)fps));
                s3d::Input::update();

                // update scene
                camera.update(&gWindow, dt);
                knife.transform()->setPosition(camera.transform()->getPosition());
                knife.transform()->setOrientation(glm::inverse(camera.transform()->getOrientation()));
                scene.update(dt);

                // update shaders
                if(fxaaShader.hasUniform("resolution"))
                    fxaaShader.setUniform("resolution", glm::vec2(gWindow.get()->getSize().x, gWindow.get()->getSize().y));
                if(gammaShader.hasUniform("gamma"))
                    gammaShader.setUniform("gamma", 1.5f);

                // render scene
                scene.render(&gWindow, {&fxaaShader, &gammaShader});
                gWindow.get()->pushGLStates();
                gWindow.get()->draw(tfps);
                gWindow.get()->display();
                gWindow.get()->popGLStates();
            }
        }
    } catch(std::exception& e) {
        std::cout << e.what() << "\n";
        std::cout << "enter anything and press ENTER to exit\n";
        std::string nothing;
        std::cin >> nothing;
    }
    s3d::Geometry::Destroy();
    return 0;
}

int main() {
    int result = context();
    gWindow.get()->close();
    return result;
}
