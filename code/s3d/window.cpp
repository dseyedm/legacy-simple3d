#include "window.h"
s3d::Window::Window() { }

void s3d::Window::initGLEW() {
    glewExperimental = GL_TRUE;
    if(glewInit() != GLEW_OK) {
        throw std::runtime_error("s3d::Window: failed to initialize GLEW");
    }
}
void s3d::Window::checkGLErrors(const s3d::Window* window) {
    if(window != nullptr) {
        window->get()->setActive();
    }
    GLenum error = glGetError();
    if(error != GL_NO_ERROR) {
        std::string errorAsString;
        switch(error) {
        case GL_INVALID_ENUM:
            errorAsString = "GL_INVALID_ENUM";      break;
        case GL_INVALID_VALUE:
            errorAsString = "GL_INVALID_VALUE";     break;
        case GL_INVALID_OPERATION:
            errorAsString = "GL_INVALID_OPERATION"; break;
        case GL_STACK_OVERFLOW:
            errorAsString = "GL_STACK_OVERFLOW";    break;
        case GL_STACK_UNDERFLOW:
            errorAsString = "GL_STACK_UNDERFLOW";   break;
        case GL_OUT_OF_MEMORY:
            errorAsString = "GL_OUT_OF_MEMORY";     break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            errorAsString = "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
        case GL_TABLE_TOO_LARGE:
            errorAsString = "GL_TABLE_TOO_LARGE";   break;
        default:
            errorAsString = "s3d_unknown_gl_error";
        }
        throw std::runtime_error("opengl error: " + errorAsString);
    }
}

const sf::RenderWindow* const s3d::Window::get() const {
    return &mWindow;
}
sf::RenderWindow* const s3d::Window::get() {
    return &mWindow;
}
