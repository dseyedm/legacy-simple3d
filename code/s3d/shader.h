#ifndef S3D_SHADER_H
#define S3D_SHADER_H

#include "util.h"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>

namespace s3d {
class Shader {
public:
    Shader();
    ~Shader();

    void destroy();

    const bool loadFromFile(const std::string& vertexFilepath, const std::string& fragmentFilepath);
    const bool loadFromMemory(const std::string& vertexSource, const std::string& fragmentSource);

    const std::string& getVertexSource() const;
    const std::string& getFragmentSource() const;
    const std::string& getVertexFilepath() const;
    const std::string& getFragmentFilepath() const;

    const GLint getUniform  (const std::string& name) const;
    const GLint getAttribute(const std::string& name) const;

    const bool hasUniform  (const std::string& name) const;
    const bool hasAttribute(const std::string& name) const;

    #define _S3D_SHADER_ATTRIB_AND_UNIFORM_SETTERS(OGL_TYPE) \
        void setAttribute(const std::string& name, OGL_TYPE v0); \
        void setAttribute(const std::string& name, OGL_TYPE v0, OGL_TYPE v1); \
        void setAttribute(const std::string& name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2); \
        void setAttribute(const std::string& name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2, OGL_TYPE v3); \
\
        void setAttribute1v(const std::string& name, const OGL_TYPE* v); \
        void setAttribute2v(const std::string& name, const OGL_TYPE* v); \
        void setAttribute3v(const std::string& name, const OGL_TYPE* v); \
        void setAttribute4v(const std::string& name, const OGL_TYPE* v); \
\
        void setUniform(const std::string& name, OGL_TYPE v0); \
        void setUniform(const std::string& name, OGL_TYPE v0, OGL_TYPE v1); \
        void setUniform(const std::string& name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2); \
        void setUniform(const std::string& name, OGL_TYPE v0, OGL_TYPE v1, OGL_TYPE v2, OGL_TYPE v3); \
\
        void setUniform1v(const std::string& name, const OGL_TYPE* v, GLsizei count=1); \
        void setUniform2v(const std::string& name, const OGL_TYPE* v, GLsizei count=1); \
        void setUniform3v(const std::string& name, const OGL_TYPE* v, GLsizei count=1); \
        void setUniform4v(const std::string& name, const OGL_TYPE* v, GLsizei count=1); \

        _S3D_SHADER_ATTRIB_AND_UNIFORM_SETTERS(GLfloat)
        _S3D_SHADER_ATTRIB_AND_UNIFORM_SETTERS(GLdouble)
        _S3D_SHADER_ATTRIB_AND_UNIFORM_SETTERS(GLint)
        _S3D_SHADER_ATTRIB_AND_UNIFORM_SETTERS(GLuint)

        void setUniformMatrix2(const std::string& name, const GLfloat* v, GLsizei count=1, GLboolean transpose=GL_FALSE);
        void setUniformMatrix3(const std::string& name, const GLfloat* v, GLsizei count=1, GLboolean transpose=GL_FALSE);
        void setUniformMatrix4(const std::string& name, const GLfloat* v, GLsizei count=1, GLboolean transpose=GL_FALSE);
        void setUniform(const std::string& name, const glm::mat2& m, GLboolean transpose=GL_FALSE);
        void setUniform(const std::string& name, const glm::mat3& m, GLboolean transpose=GL_FALSE);
        void setUniform(const std::string& name, const glm::mat4& m, GLboolean transpose=GL_FALSE);
        void setUniform(const std::string& name, const glm::vec2& v);
        void setUniform(const std::string& name, const glm::vec3& v);
        void setUniform(const std::string& name, const glm::vec4& v);

    void bind();
    const bool bound() const;
    static void unbind();
    const GLuint& getObject() const;
private:
    static GLint mCurrentProgram;

    const GLint getSafeUniform(const std::string& name) const;
    const GLint getSafeAttribute(const std::string& name) const;

    void safeBind();
    void safeUnbind();

    const std::string getError() const;
    const GLuint getErrorType() const;

    void setObject(const GLuint obj);

    const bool compile(GLuint *shaderObj, const std::string& code, const GLenum type);
    const bool link(GLuint *shaderObj);
    void deleteObjects();
    void resetVariables();

    GLuint mObject;
    GLuint mVertexObject;
    GLuint mFragmentObject;
    std::string mVertexSource;
    std::string mFragmentSource;
    std::string mVertexFilepath;
    std::string mFragmentFilepath;
    std::string mError;
    GLuint mErrorType;
};
};

#endif // S3D_SHADER_H
