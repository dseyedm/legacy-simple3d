#ifndef GLW_WINDOW_H
#define GLW_WINDOW_H

#include <GL/glew.h>
#include <SFML/Graphics.hpp>

#include <stdexcept>

namespace s3d {
class Window {
public:
    Window();

    void initGLEW();
    static void checkGLErrors(const s3d::Window* window = nullptr);

    const sf::RenderWindow* const get() const;
    sf::RenderWindow* const get();
private:
    sf::RenderWindow mWindow;
};
};

#endif // GLW_WINDOW_H
