#include "model.h"
#include "scene.h"

s3d::Model::Model() {
    setCulling(true);
    setWireframe(false);
    setDepthTest(true);
    setDepthMask(true);
    setDepthClear(false);
}

const bool s3d::Model::getCulling() const {
    return mCulling;
}
void s3d::Model::setCulling(const bool culling) {
    mCulling = culling;
}
const bool s3d::Model::getWireframe() const {
    return mWireframe;
}
void s3d::Model::setWireframe(const bool wireframe) {
    mWireframe = wireframe;
}
const bool s3d::Model::getDepthTest() const {
    return mDepthTest;
}
void s3d::Model::setDepthTest(const bool test) {
    mDepthTest = test;
}
const bool s3d::Model::getDepthMask() const {
    return mDepthMask;
}
void s3d::Model::setDepthMask(const bool mask) {
    mDepthMask = mask;
}
const bool s3d::Model::getDepthClear() const {
    return mDepthClear;
}
void s3d::Model::setDepthClear(const bool clear) {
    mDepthClear = clear;
}

std::string s3d::Model::mModelTransformStr;
std::string s3d::Model::mModelScaleStr;
std::string s3d::Model::mModelRotationStr;
std::string s3d::Model::mModelPositionStr;

void s3d::Model::setModelTransformStr(const std::string& str) {
    mModelTransformStr = str;
}
const std::string& s3d::Model::getModelTransformStr() {
    if(mModelTransformStr.size() <= 0) {
        setModelTransformStr();
    }
    return mModelTransformStr;
}
void s3d::Model::setModelScaleStr(const std::string& str) {
    mModelScaleStr = str;
}
const std::string& s3d::Model::getModelScaleStr() {
    if(mModelScaleStr.size() <= 0) {
        setModelScaleStr();
    }
    return mModelScaleStr;
}
void s3d::Model::setModelRotationStr(const std::string& str) {
    mModelRotationStr = str;
}
const std::string& s3d::Model::getModelRotationStr() {
    if(mModelRotationStr.size() <= 0) {
        setModelRotationStr();
    }
    return mModelRotationStr;
}
void s3d::Model::setModelPositionStr(const std::string& str) {
    mModelPositionStr = str;
}
const std::string& s3d::Model::getModelPositionStr() {
    if(mModelPositionStr.size() <= 0) {
        setModelPositionStr();
    }
    return mModelPositionStr;
}

// StaticModel
s3d::StaticModel::StaticModel() {
    setGeometry(nullptr);
    setMaterial(nullptr);
}
void s3d::StaticModel::update(const s3d::Scene* const scene, const float dt) {
    ;
}
void s3d::StaticModel::render(const s3d::Scene* const scene) {
    if(hasMaterial()) {
        material()->bind();
        if(material()->hasShader()) {
            if(scene != nullptr && scene->hasCamera()) {
                scene->camera()->attach(material()->shader());
            }
            if(material()->shader()->hasUniform(getModelTransformStr())) {
                material()->shader()->setUniform(getModelTransformStr(), transform()->calculate());
            }
            if(material()->shader()->hasUniform(getModelScaleStr())) {
                material()->shader()->setUniform(getModelScaleStr(), transform()->getScale());
            }
            if(material()->shader()->hasUniform(getModelRotationStr())) {
                material()->shader()->setUniform(getModelRotationStr(), transform()->getRotation());
            }
            if(material()->shader()->hasUniform(getModelPositionStr())) {
                material()->shader()->setUniform(getModelPositionStr(), transform()->getPosition());
            }
            if(hasGeometry()) {
                geometry()->attach(material()->shader());
                geometry()->render();
            }
        }
        material()->unbind();
    }
}

const glm::vec3 s3d::StaticModel::getSize() const {
    if(hasGeometry()) {
        return geometry()->getSize() * transform()->getScale();
    }
    return glm::vec3(0);
}

const s3d::Transform* s3d::StaticModel::transform() const {
    return &mTransform;
}
s3d::Transform* s3d::StaticModel::transform() {
    return &mTransform;
}

s3d::Geometry* s3d::StaticModel::geometry() const {
    s3d_assert(hasGeometry());
    return mGeometry;
}
void s3d::StaticModel::setGeometry(s3d::Geometry* const geometry) {
    mGeometry = geometry;
}
const bool s3d::StaticModel::hasGeometry() const {
    return (mGeometry != nullptr);
}

s3d::Material* s3d::StaticModel::material() const {
    s3d_assert(hasMaterial());
    return mMaterial;
}
void s3d::StaticModel::setMaterial(s3d::Material* const material) {
    mMaterial = material;
}
const bool s3d::StaticModel::hasMaterial() const {
    return (mMaterial != nullptr);
}
