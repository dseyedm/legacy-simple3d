#ifndef S3D_UTIL_H
#define S3D_UTIL_H

#include "debug.h"

#include <glm/glm.hpp>

#include <sstream>
#include <string>
#include <fstream>
#include <stdexcept>
#include <iostream>

namespace s3d {
namespace util {
    const bool fileExists(const std::string& filepath);
    const std::string loadFile(const std::string& filepath);
    template <class T>
    const std::string toString(T t);
    const std::string toString(const glm::vec3& vec);
};
};

template <class T>
const std::string s3d::util::toString(T t) {
    std::stringstream ss;
    ss << t;
    return ss.str();
}

#endif // S3D_UTIL_H
