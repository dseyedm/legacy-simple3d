#include "scene.h"

s3d::Scene::Scene() {
    mCamera = nullptr;

    mQuad.setMaterial(&mQuadMaterial);
    mQuad.setGeometry(&s3d::Geometry::Quad);
    mQuad.setDepthTest(false);
}

void s3d::Scene::update(const float dt) {
    for(unsigned int i=0;i<mModels.size();i++) {
        mModels.at(i)->update(this, dt);
    }
}

void s3d::Scene::render(const s3d::Window* window, const std::vector<s3d::Shader*>& shaders) {
    window->get()->setActive();
    updateClearColor();
    if(!shaders.empty()) {
        // (re)create the framebuffer if window has changed size
        const glm::vec2 windowSize(window->get()->getSize().x, window->get()->getSize().y);
        if(mFramebufferSize != windowSize) {
            mFramebufferSize = windowSize;
            mFramebufferA.create(mFramebufferSize, s3d::Framebuffer::Type::Color);
            mFramebufferB.create(mFramebufferSize, s3d::Framebuffer::Type::Color);
        }

        // clear buffers and render
        mFramebufferA.bind();
            enableAlpha();
            clearBuffers();
            renderModels();

        bool writeBufferA = false;
        for(unsigned int i=0;i<shaders.size();i++) {
            s3d::Texture* source = nullptr;
            if(writeBufferA) {
                mFramebufferA.bind();
                source = mFramebufferB.texture();
            } else {
                mFramebufferB.bind();
                source = mFramebufferA.texture();
            }
            if(i + 1 >= shaders.size()) {
                s3d::Framebuffer::unbind();
            }

            mQuadMaterial.removeTextures();
            mQuadMaterial.addTexture(source, "frame_texture");
            mQuadMaterial.setShader(shaders.at(i));
            renderModel(&mQuad);

            writeBufferA = !writeBufferA;
        }
    } else {
        enableAlpha();
        clearBuffers();
        renderModels();
    }

    // disable wireframe for sfml compatibility
    if(mWireframe == true) {
        setWireframe(false);
    }
}

s3d::Camera* s3d::Scene::camera() const {
    s3d_assert(hasCamera());
    return mCamera;
}
void s3d::Scene::setCamera(s3d::Camera* const camera) {
    mCamera = camera;
}
const bool s3d::Scene::hasCamera() const {
    return (mCamera != nullptr);
}

void s3d::Scene::addModel(s3d::Model* const model) {
    s3d_assert(model != nullptr);
    mModels.push_back(model);
}

void s3d::Scene::removeModel(const s3d::Model* const model) {
    for(unsigned int i=0;i<mModels.size();i++) {
        if(model == mModels.at(i)) {
            mModels.erase(mModels.begin() + i);
            // break out of loop
            i = mModels.size();
        }
    }
}
const bool s3d::Scene::hasModel(const s3d::Model* const model) const {
    for(unsigned int i=0;i<mModels.size();i++) {
        if(model == mModels.at(i)) {
            return true;
        }
    }
    return false;
}
void s3d::Scene::updateClearColor() {
    if(hasCamera()) {
        glClearColor(camera()->getClearColor().x, camera()->getClearColor().y, camera()->getClearColor().z, 1);
    } else {
        glClearColor(0, 0, 0, 1);
    }
}
void s3d::Scene::clearBuffers() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
void s3d::Scene::clearColor() {
    glClear(GL_COLOR_BUFFER_BIT);
}
void s3d::Scene::clearDepth() {
    glClear(GL_DEPTH_BUFFER_BIT);
}
void s3d::Scene::renderModel(s3d::Model* const model) {
    if(model->getCulling() != mCulling) {
        setCulling(model->getCulling());
    }
    if(model->getWireframe() != mWireframe) {
        setWireframe(model->getWireframe());
    }
    if(model->getDepthTest() != mDepthTest) {
        setDepthTest(model->getDepthTest());
    }
    if(model->getDepthMask() != mDepthMask) {
        setDepthMask(model->getDepthMask());
    }
    if(model->getDepthClear()) {
        clearDepth();
    }
    model->render(this);
}
void s3d::Scene::renderModels() {
    if(hasCamera()) {
        for(unsigned int i=0;i<mModels.size();i++) {
            renderModel(mModels.at(i));
        }
    }
}

void s3d::Scene::setCulling(const bool culling) {
    if(culling) {
        glEnable(GL_CULL_FACE);
    } else {
        glDisable(GL_CULL_FACE);
    }
    mCulling = culling;
}
void s3d::Scene::setWireframe(const bool wireframe) {
    if(wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    mWireframe = wireframe;
}
void s3d::Scene::setDepthTest(const bool test) {
    if(test) {
        glEnable(GL_DEPTH_TEST);
    } else {
        glDisable(GL_DEPTH_TEST);
    }
    mDepthTest = test;
}
void s3d::Scene::setDepthMask(const bool mask) {
    if(mask) {
        glDepthMask(GL_TRUE);
    } else {
        glDepthMask(GL_FALSE);
    }
    mDepthMask = mask;
}

void s3d::Scene::enableAlpha() {
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
}
