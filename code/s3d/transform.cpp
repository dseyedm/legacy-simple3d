#include "transform.h"

const s3d::Transform s3d::Transform::interpolate(const s3d::Transform& t1, const s3d::Transform& t2, const float alpha) {
    s3d::Transform interpolated;
    interpolated.setOrientation(glm::slerp(t1.getOrientation(), t2.getOrientation(), alpha));
    interpolated.setPosition(glm::mix(t1.getPosition(), t2.getPosition(), alpha));
    interpolated.setScale(glm::mix(t1.getScale(), t2.getScale(), alpha));
    return interpolated;
}

s3d::Transform::Transform() {
    setPosition(glm::vec3(0));
    setScale(glm::vec3(1));
}

void s3d::Transform::set(const s3d::Transform& t) {
    setOrientation(t.getOrientation());
    setPosition(t.getPosition());
    setScale(t.getScale());
}

const glm::mat4 s3d::Transform::calculate() const {
    return getPositionMat4() * getRotationMat4() * getScaleMat4();
}

void s3d::Transform::face(const glm::vec3& point) {
    if(point != getPosition()) {
        glm::vec3 direction = glm::normalize(point - getPosition());
        float up = glm::degrees(asinf(-direction.y));
        float right = -glm::degrees(atan2f(-direction.x, -direction.z));

        setOrientation(glm::quat());
        rotate(right, glm::vec3(0, 1, 0));
        rotate(up, this->rightInverse());
    }
}

void s3d::Transform::faceInverse(const glm::vec3& point) {
    if(point != getPosition()) {
        glm::vec3 direction = glm::normalize(point - getPosition());
        float up = glm::degrees(asinf(-direction.y));
        float right = -glm::degrees(atan2f(-direction.x, -direction.z));

        setOrientation(glm::quat());
        rotate(right, glm::vec3(0, 1, 0));
        rotate(up, this->rightInverse());
        setOrientation(glm::inverse(getOrientation()));
    }
}

void s3d::Transform::setPosition(const glm::vec3& position) {
    mPosition = position;
}
void s3d::Transform::setScale(const glm::vec3& scale) {
    mScale = scale;
}
void s3d::Transform::setOrientation(const glm::quat& orientation) {
    mOrientation = orientation;
}

void s3d::Transform::translate(const glm::vec3& offset) {
    mPosition += offset;
}
void s3d::Transform::scale(const glm::vec3& offset) {
    mScale *= offset;
}
void s3d::Transform::rotate(const float degree, const glm::vec3 axis) {
    glm::vec3 normalAxis = glm::normalize(axis);
    glm::quat offset = glm::angleAxis(degree, normalAxis);
    setOrientation(getOrientation() * offset);
}

const glm::vec3& s3d::Transform::getPosition() const {
    return mPosition;
}
const glm::vec3& s3d::Transform::getScale() const {
    return mScale;
}
const glm::quat& s3d::Transform::getOrientation() const {
    return mOrientation;
}
const glm::vec3 s3d::Transform::getRotation() const {
    glm::vec3 rot = glm::eulerAngles(getOrientation());
    if(rot.x < 0) { rot.x += 360; }
    if(rot.y < 0) { rot.y += 360; }
    if(rot.z < 0) { rot.z += 360; }
    rot.x = 360 - rot.x;
    rot.y = 360 - rot.y;
    rot.z = 360 - rot.z;
    return rot;
}

const glm::mat4 s3d::Transform::getPositionMat4() const {
    glm::mat4 position = glm::translate(glm::mat4(1), getPosition());
    return position;
}
const glm::mat4 s3d::Transform::getRotationMat4() const {
    return glm::toMat4(getOrientation());
}
const glm::mat4 s3d::Transform::getScaleMat4() const {
    glm::mat4 scale = glm::scale(glm::mat4(1), getScale());
    return scale;
}

const glm::vec3 s3d::Transform::right() const {
    glm::mat4 mat = getRotationMat4();
    return glm::vec3(mat * glm::vec4(1, 0, 0, 0));
}
const glm::vec3 s3d::Transform::up() const {
    glm::mat4 mat = getRotationMat4();
    return glm::vec3(mat * glm::vec4(0, 1, 0, 0));
}
const glm::vec3 s3d::Transform::forward() const {
    glm::mat4 mat = getRotationMat4();
    return glm::vec3(mat * glm::vec4(0, 0, -1, 0));
}
const glm::vec3 s3d::Transform::rightInverse() const {
    glm::mat4 mat = glm::inverse(getRotationMat4());
    return glm::vec3(mat * glm::vec4(1, 0, 0, 0));
}
const glm::vec3 s3d::Transform::upInverse() const {
    glm::mat4 mat = glm::inverse(getRotationMat4());
    return glm::vec3(mat * glm::vec4(0, 1, 0, 0));
}
const glm::vec3 s3d::Transform::forwardInverse() const {
    glm::mat4 mat = glm::inverse(getRotationMat4());
    return glm::vec3(mat * glm::vec4(0, 0, -1, 0));
}

const bool s3d::Transform::operator == (const s3d::Transform& t) const {
    return (getPosition()    == t.getPosition() &&
            getScale()       == t.getScale() &&
            getOrientation() == t.getOrientation());
}
const bool s3d::Transform::operator != (const s3d::Transform& t) const {
    return !(*this == t);
}
const s3d::Transform& s3d::Transform::operator = (const s3d::Transform& t) {
    set(t);
    return *this;
}
