#ifndef S3D_MODEL_H
#define S3D_MODEL_H

#include "transform.h"
#include "geometry.h"
#include "material.h"

namespace s3d {
class Scene;
class Model {
protected:
    Model();
    virtual ~Model() {}
public:
    virtual void update(const s3d::Scene* const scene, const float dt) = 0;
    virtual void render(const s3d::Scene* const scene = nullptr) = 0;

    const bool getCulling() const;
    void setCulling(const bool culling);
    const bool getWireframe() const;
    void setWireframe(const bool wireframe);
    const bool getDepthTest() const;
    void setDepthTest(const bool test);
    const bool getDepthMask() const;
    void setDepthMask(const bool mask);
    const bool getDepthClear() const;
    void setDepthClear(const bool clear);
private:
    bool mCulling;
    bool mWireframe;
    bool mDepthTest;
    bool mDepthMask;
    bool mDepthClear;

    static std::string mModelTransformStr;
    static std::string mModelScaleStr;
    static std::string mModelRotationStr;
    static std::string mModelPositionStr;
public:
    static void setModelTransformStr(const std::string& str = "s3d_model_transform");
    static const std::string& getModelTransformStr();
    static void setModelScaleStr(const std::string& str = "s3d_model_scale");
    static const std::string& getModelScaleStr();
    static void setModelRotationStr(const std::string& str = "s3d_model_rotation");
    static const std::string& getModelRotationStr();
    static void setModelPositionStr(const std::string& str = "s3d_model_position");
    static const std::string& getModelPositionStr();
};
class StaticModel : public Model {
public:
    StaticModel();

    void update(const s3d::Scene* const scene, const float dt) override;
    void render(const s3d::Scene* const scene = nullptr) override;

    const glm::vec3 getSize() const;
    const s3d::Transform* transform() const;
    s3d::Transform* transform();

    s3d::Geometry* geometry() const;
    void setGeometry(s3d::Geometry* const geometry);
    const bool hasGeometry() const;

    Material* material() const;
    void setMaterial(s3d::Material* const material);
    const bool hasMaterial() const;

private:
    s3d::Transform mTransform;
    s3d::Geometry* mGeometry;
    s3d::Material* mMaterial;
};
}

#endif // S3D_MODEL_H
