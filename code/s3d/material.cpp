#include "material.h"

s3d::Material::Material() {
    setShader(nullptr);
}

void s3d::Material::bind() const {
    if(hasShader()) {
        shader()->bind();
        for(mTextureIterator it = mTextures.begin(); it != mTextures.end(); ++it) {
            const int unit = std::distance(mTextures.begin(), it);
            shader()->setUniform(it->first, unit);
            it->second->bind(unit);
        }
    }
}
void s3d::Material::unbind() const {
    s3d::Shader::unbind();
    s3d::Texture2d::unbind();
    s3d::Texture3d::unbind();
}

s3d::Shader* s3d::Material::shader() const {
    s3d_assert(hasShader());
    return mShader;
}
void s3d::Material::setShader(s3d::Shader* const shader) {
    mShader = shader;
}
const bool s3d::Material::hasShader() const {
    return (mShader != nullptr);
}

void s3d::Material::addTexture(const s3d::Texture* const texture, const std::string& shaderVariable) {
    if(texture != nullptr) {
        if(!hasTexture(shaderVariable)) {
            mTextures.emplace(shaderVariable, texture);
        }
    }
}
void s3d::Material::removeTexture(const std::string& shaderVariable) {
    if(hasTexture(shaderVariable)) {
        mTextures.erase(shaderVariable);
    }
}
void s3d::Material::removeTextures() {
    mTextures.clear();
}
const bool s3d::Material::hasTexture(const std::string& shaderVariable) const {
    return (mTextures.find(shaderVariable) != mTextures.end());
}
const bool s3d::Material::hasTexture(const s3d::Texture* const texture) const {
    for(mTextureIterator it = mTextures.begin(); it != mTextures.end(); ++it) {
        if(it->second == texture) {
            return true;
        }
    }
    return false;
}
const s3d::Texture* const s3d::Material::getTexture(const std::string& shaderVariable) const {
    if(hasTexture(shaderVariable)) {
        return (mTextures.at(shaderVariable));
    }
    return nullptr;
}
