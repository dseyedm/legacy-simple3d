#include "camera.h"

s3d::Camera::Camera() {
    setClearColor(glm::vec3(0.1));
    setFieldOfView(45);
    setNearFar(1.0f, 10.0f);
    transform()->setPosition(glm::vec3(0));
    setSensitivity(0.018, 0.016);
}
s3d::Transform* s3d::Camera::transform() {
    return &mTransform;
}
const s3d::Transform* s3d::Camera::transform() const {
    return &mTransform;
}
s3d::Input* s3d::Camera::input() {
    return &mInput;
}
const s3d::Input* s3d::Camera::input() const {
    return &mInput;
}

void s3d::Camera::setClearColor(const glm::vec3& color) {
    mClearColor = color;
}
const glm::vec3& s3d::Camera::getClearColor() const {
    return mClearColor;
}
void s3d::Camera::setFieldOfView(const float fieldOfView) {
    s3d_assert(fieldOfView > 0.0f && fieldOfView < 180.0f);
    mFieldOfView = fieldOfView;
}
const float s3d::Camera::getFieldOfView() const {
    return mFieldOfView;
}
void s3d::Camera::setAspectRatio(const float ratio) {
    s3d_assert(ratio > 0.0f);
    mAspectRatio = ratio;
}
const float s3d::Camera::getAspectRatio() const {
    return mAspectRatio;
}
void s3d::Camera::setNearFar(const float nearPlane, const float farPlane) {
    s3d_assert(nearPlane > 0.0f);
    s3d_assert(farPlane > nearPlane);
    mNear = nearPlane;
    mFar  = farPlane;
}
const float s3d::Camera::getNear() const {
    return mNear;
}
const float s3d::Camera::getFar() const {
    return mFar;
}
void s3d::Camera::look(s3d::Window* const window, const float dt) {
    window->get()->setMouseCursorVisible(false);
    sf::Vector2i mouse = sf::Mouse::getPosition(*window->get());
    sf::Vector2u resolution = window->get()->getSize();

    const float lookUp = -float((float(resolution.y / 2.f) - mouse.y) * mSensY);
    const float lookRight = float((mouse.x - float(resolution.x / 2.f)) * mSensX);

    transform()->rotate(lookRight, glm::vec3(0, 1, 0));
    transform()->rotate(lookUp, transform()->rightInverse());

    sf::Mouse::setPosition(sf::Vector2i(resolution.x/2, resolution.y/2), *window->get());
}
void s3d::Camera::move(s3d::Window* const window, const float dt) {
    const float speed = 10 * dt;
    if(input()->keyPressed(sf::Keyboard::Key::W)) {
        transform()->translate(transform()->forwardInverse() * speed);
    }
    if(input()->keyPressed(sf::Keyboard::Key::S)) {
        transform()->translate(-transform()->forwardInverse() * speed);
    }
    if(input()->keyPressed(sf::Keyboard::Key::A)) {
        transform()->translate(-transform()->rightInverse() * speed);
    }
    if(input()->keyPressed(sf::Keyboard::Key::D)) {
        transform()->translate(transform()->rightInverse() * speed);
    }
    if(input()->keyPressed(sf::Keyboard::Key::Z) || input()->keyPressed(sf::Keyboard::Key::Space)) {
        transform()->translate(glm::vec3(0, speed, 0));
    }
    if(input()->keyPressed(sf::Keyboard::Key::X) || input()->keyPressed(sf::Keyboard::Key::LControl)) {
        transform()->translate(glm::vec3(0, -speed, 0));
    }
}
void s3d::Camera::update(s3d::Window* const window, const float dt) {
    if(input()->mousePressed(sf::Mouse::Button::Right)) {
        look(window, dt);
        move(window, dt);
    }
}
void s3d::Camera::attach(s3d::Shader* const shader) const {
    if(shader != nullptr) {
        if(shader->hasUniform(getViewStr())) shader->setUniform(getViewStr(), view());
        if(shader->hasUniform(getProjStr())) shader->setUniform(getProjStr(), proj());
        if(shader->hasUniform(getOrientationStr())) shader->setUniform(getOrientationStr(), transform()->getRotationMat4());
        if(shader->hasUniform(getPosStr())) shader->setUniform(getPosStr(), transform()->getPosition());
        if(shader->hasUniform(getUpStr())) shader->setUniform(getUpStr(), transform()->upInverse());
        if(shader->hasUniform(getRightStr())) shader->setUniform(getRightStr(), transform()->rightInverse());
        if(shader->hasUniform(getForwardStr())) shader->setUniform(getForwardStr(), transform()->forwardInverse());
    }
}
void s3d::Camera::setSensitivity(const float x, const float y) {
    mSensX = x;
    mSensY = y;
}
const float s3d::Camera::getSensitivityX() const {
    return mSensX;
}
const float s3d::Camera::getSensitivityY() const {
    return mSensY;
}
const glm::mat4 s3d::Camera::proj() const {
    return glm::perspective(getFieldOfView(), getAspectRatio(), getNear(), getFar());
}
const glm::mat4 s3d::Camera::view() const {
    return transform()->getRotationMat4() * glm::translate(glm::mat4(), -transform()->getPosition());
}

// shader strings
void s3d::Camera::setViewStr(const std::string& str) {
    mViewStr = str;
}
void s3d::Camera::setProjStr(const std::string& str) {
    mProjStr = str;
}
void s3d::Camera::setOrientationStr(const std::string& str) {
    mOrientationStr = str;
}
void s3d::Camera::setPosStr(const std::string& str) {
    mPosStr = str;
}
void s3d::Camera::setRightStr(const std::string& str) {
    mRightStr = str;
}
void s3d::Camera::setUpStr(const std::string& str) {
    mUpStr = str;
}
void s3d::Camera::setForwardStr(const std::string& str) {
    mForwardStr = str;
}
const std::string& s3d::Camera::getViewStr() {
    if(mViewStr.size() <= 0) {
        setViewStr();
    }
    return mViewStr;
}
const std::string& s3d::Camera::getProjStr() {
    if(mProjStr.size() <= 0) {
        setProjStr();
    }
    return mProjStr;
}
const std::string& s3d::Camera::getOrientationStr() {
    if(mOrientationStr.size() <= 0) {
        setOrientationStr();
    }
    return mOrientationStr;
}
const std::string& s3d::Camera::getPosStr() {
    if(mPosStr.size() <= 0) {
        setPosStr();
    }
    return mPosStr;
}
const std::string& s3d::Camera::getRightStr() {
    if(mRightStr.size() <= 0) {
        setRightStr();
    }
    return mRightStr;
}
const std::string& s3d::Camera::getUpStr() {
    if(mUpStr.size() <= 0) {
        setUpStr();
    }
    return mUpStr;
}
const std::string& s3d::Camera::getForwardStr() {
    if(mForwardStr.size() <= 0) {
        setForwardStr();
    }
    return mForwardStr;
}
std::string s3d::Camera::mViewStr;
std::string s3d::Camera::mProjStr;
std::string s3d::Camera::mOrientationStr;
std::string s3d::Camera::mPosStr;
std::string s3d::Camera::mRightStr;
std::string s3d::Camera::mUpStr;
std::string s3d::Camera::mForwardStr;
