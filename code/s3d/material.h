#ifndef S3D_MATERIAL_H
#define S3D_MATERIAL_H

#include "shader.h"
#include "texture.h"

#include <map>

namespace s3d {
class Material {
public:
    Material();

    void bind() const;
    void unbind() const;

    s3d::Shader* shader() const;
    void setShader(s3d::Shader* const shader);
    const bool hasShader() const;

    void addTexture(const s3d::Texture* const texture, const std::string& shaderVariable);
    void removeTexture(const std::string& shaderVariable);
    void removeTextures();
    const bool hasTexture(const std::string& shaderVariable) const;
    const bool hasTexture(const s3d::Texture* const texture) const;
    const s3d::Texture* const getTexture(const std::string& shaderVariable) const;
private:
    s3d::Shader* mShader;
    std::map<const std::string, const s3d::Texture* const> mTextures;
    typedef std::map<const std::string, const s3d::Texture* const>::const_iterator mTextureIterator;
};
}

#endif // S3D_MATERIAL_H
