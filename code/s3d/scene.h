#ifndef S3D_SCENE_H
#define S3D_SCENE_H

#include "camera.h"
#include "model.h"
#include "framebuffer.h"

namespace s3d {
class Scene {
public:
    Scene();

    void destroy();
    void update(const float dt);
    void render(const s3d::Window* const window, const std::vector<s3d::Shader*>& shaders = std::vector<s3d::Shader*>());

    s3d::Camera* camera() const;
    void setCamera(s3d::Camera* const camera);
    const bool hasCamera() const;

    void addModel(s3d::Model* const model);
    void removeModel(const s3d::Model* const model);
    const bool hasModel(const s3d::Model* const model) const;
private:
    void updateClearColor();
    void clearBuffers();
    void clearColor();
    void clearDepth();
    void renderModel(s3d::Model* const model);
    void renderModels();

    void setCulling(const bool culling);
    void setWireframe(const bool wireframe);
    void setDepthTest(const bool test);
    void setDepthMask(const bool mask);
    void enableAlpha();

    s3d::Camera* mCamera;
    std::vector<s3d::Model*> mModels;
    s3d::Framebuffer mFramebufferA;
    s3d::Framebuffer mFramebufferB;
    glm::vec2 mFramebufferSize;

    s3d::StaticModel mQuad;
    s3d::Material mQuadMaterial;

    bool mCulling;
    bool mWireframe;
    bool mDepthTest;
    bool mDepthMask;
};
}

#endif // S3D_SCENE_H
