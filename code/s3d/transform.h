#ifndef S3D_TRANSFORM_H
#define S3D_TRANSFORM_H

#include "util.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

namespace s3d {
class Transform {
public:
    static const Transform interpolate(const Transform& t1, const Transform& t2, const float alpha);

    Transform();

    void set(const Transform& t);

    const glm::mat4 calculate() const;

    void face(const glm::vec3& point);
    void faceInverse(const glm::vec3& point);

    void setPosition(const glm::vec3& position);
    void setScale(const glm::vec3& scale);
    void setOrientation(const glm::quat& orientation);

    void translate(const glm::vec3& offset);
    void scale(const glm::vec3& offset);
    void rotate(const float degree, const glm::vec3 axis);

    const glm::vec3& getPosition() const;
    const glm::vec3& getScale() const;
    const glm::quat& getOrientation() const;
    const glm::vec3  getRotation() const;

    const glm::mat4 getPositionMat4() const;
    const glm::mat4 getRotationMat4() const;
    const glm::mat4 getScaleMat4() const;

    const glm::vec3 right() const;
    const glm::vec3 up() const;
    const glm::vec3 forward() const;
    const glm::vec3 rightInverse() const;
    const glm::vec3 upInverse() const;
    const glm::vec3 forwardInverse() const;

    const bool operator == (const Transform& t) const;
    const bool operator != (const Transform& t) const;
    const Transform& operator = (const Transform& t);
private:
    glm::vec3 mPosition;
    glm::vec3 mScale;
    glm::quat mOrientation;
};
}

#endif // S3D_TRANSFORM_H
