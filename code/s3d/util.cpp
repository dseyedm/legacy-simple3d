#include "util.h"
#include "debug.h"

const bool s3d::util::fileExists(const std::string& filepath) {
    std::ifstream file(filepath.c_str());
    return file;
}
const std::string s3d::util::loadFile(const std::string& filepath) {
    if(!fileExists(filepath))
        throw std::runtime_error("file '" + filepath + "' does not exist");

    std::ifstream filestream(filepath.c_str());
    if(!filestream)
        throw std::runtime_error(s3d::debug::failedToLoad(filepath));

    std::stringstream data;
    data << filestream.rdbuf();
    filestream.close();
    return data.str();
}
const std::string s3d::util::toString(const glm::vec3& vec) {
    return ("(" + toString(vec.x) + "," + toString(vec.y) + "," + toString(vec.z) + ")");
}
