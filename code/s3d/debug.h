#ifndef S3D_DEBUG_H
#define S3D_DEBUG_H

#include "util.h"

#include <vector>
#include <string>

namespace s3d {
namespace debug {
    void s3d_assert_(const char* const condition, const char* const filename, const int line, const char* const function);
    #define s3d_assert(c) ((c) ? (void)0 : s3d::debug::s3d_assert_(#c, __FILE__, __LINE__, __FUNCTION__))
    const std::string failedToLoad(const std::string& filepath);
    const std::string noElementExists(const std::string& name);
    const std::string elementExists(const std::string& name);
}
}

#endif // S3D_DEBUG_H
