#ifndef S3D_INPUT_H
#define S3D_INPUT_H

#include <SFML/Graphics.hpp>

namespace s3d {
class Input {
public:
    Input();
    void setEnabled(const bool enabled);
    const bool isEnabled() const;

    const bool keyPressed   (const sf::Keyboard::Key key) const;
    const bool keyWasPressed(const sf::Keyboard::Key key) const;
    const bool keyTapped    (const sf::Keyboard::Key key) const;

    const bool mousePressed   (const sf::Mouse::Button button) const;
    const bool mouseWasPressed(const sf::Mouse::Button button) const;
    const bool mouseTapped    (const sf::Mouse::Button button) const;
private:
    bool mEnabled;

public:
    static void setGlobalEnabled(const bool enabled);
    static const bool isGlobalEnabled();
    static void update();
private:
    static std::vector<bool> mKeyPressed;
    static std::vector<bool> mWasKeyPressed;
    static std::vector<bool> mButtonPressed;
    static std::vector<bool> mWasButtonPressed;

    static bool mGlobalEnabled;
};
}

#endif // S3D_INPUT_H
