#include "texture.h"

s3d::Texture::Texture() {
    setObject(0);
}

const int s3d::Texture::getMaximumUnits() {
    static GLint max = -1;
    if(max < 0) {
        glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &max);
    }
    return max;
}

const GLuint& s3d::Texture::getObject() const {
    return mObject;
}
const bool s3d::Texture::hasObject() const {
    return (getObject() > 0);
}
const bool s3d::Texture::bound() const {
    GLint boundTexture;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &boundTexture);
    return (boundTexture == (GLint)getObject());
}
void s3d::Texture::setObject(const GLuint object) {
    mObject = object;
}

// Texture2d
s3d::Texture2d::Texture2d() {}
s3d::Texture2d::~Texture2d() {
    destroy();
}

void s3d::Texture2d::loadFromFile(const std::string& path) {
    try {
        destroy();

        if(!s3d::util::fileExists(path)) {
            throw std::runtime_error(s3d::debug::failedToLoad(path));
        }

        GLint newObject = SOIL_load_OGL_texture(
            path.c_str(),
            SOIL_LOAD_AUTO,
            SOIL_CREATE_NEW_ID,
            SOIL_FLAG_INVERT_Y
        );
        if(newObject == 0) {
            throw std::runtime_error((std::string)"SOIL failed to load '" + path + "': " + SOIL_last_result());
        }
        setObject(newObject);
        setWrapping(Wrapping::Clamp);
        setFiltering(Filtering::Linear);
    } catch(std::exception& e) {
        throw std::runtime_error((std::string)"s3d::Texture2d::loadFromFile: " + e.what());
    }
}

void s3d::Texture2d::destroy() {
    if(hasObject()) {
        glDeleteTextures(1, &getObject());
        setObject(0);
    }
}

void s3d::Texture2d::setWrapping(const Wrapping& wrap) {
    if(hasObject()) {
        bind();
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (GLint)wrap);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (GLint)wrap);
            mWrapping = wrap;
        unbind();
    }
}
void s3d::Texture2d::setFiltering(const Filtering& filtering, const Mode& mode) {
    if(hasObject()) {
        bind();
            if(mode == Mode::Near || mode == Mode::Both) {
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLint)filtering);
                mFilteringNear = filtering;
            }
            if(mode == Mode::Far || mode == Mode::Both) {
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLint)filtering);
                mFilteringFar = filtering;
            }
        unbind();
    }
}
void s3d::Texture2d::addFilter(const Filter& filter) {
    if(hasObject()) {
        bind();
            if(filter == Filter::Anisotropic) {
                if(glewIsSupported("GL_EXT_texture_filter_anisotropic")) {
                    GLfloat max;
                    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max);
                    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, max);
                }
            }
        unbind();
    }
}
void s3d::Texture2d::removeFilter(const Filter& filter) {
    if(hasObject()) {
        bind();
            if(filter == Filter::Anisotropic) {
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 1.0f);
            }
        unbind();
    }
}

const s3d::Texture::Wrapping& s3d::Texture2d::getWrapping() const {
    return mWrapping;
}
const s3d::Texture::Filtering& s3d::Texture2d::getFilteringNear() const {
    return mFilteringNear;
}
const s3d::Texture::Filtering& s3d::Texture2d::getFilteringFar() const {
    return mFilteringFar;
}

void s3d::Texture2d::bind(const int unit) const {
    if(hasObject()) {
        glEnable(GL_TEXTURE_2D);
        if((GLint)unit < getMaximumUnits()) {
            glActiveTexture(GL_TEXTURE0 + unit);
        } else {
            s3d_assert("requested texture unit outside of supported range" == 0);
        }
        glBindTexture(GL_TEXTURE_2D, getObject());
        glDisable(GL_TEXTURE_2D);
    }
}
void s3d::Texture2d::unbind() {
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE0);
}

// Texture3d
s3d::Texture3d::Texture3d() {}
s3d::Texture3d::~Texture3d() {
    destroy();
}

void s3d::Texture3d::loadFromFile(const std::string& path, const std::string& order) {
    try {
        destroy();

        if(!s3d::util::fileExists(path)) {
            throw std::runtime_error(s3d::debug::failedToLoad(path));
        }

        std::string orderCapitals = order;
        std::transform(orderCapitals.begin(), orderCapitals.end(), orderCapitals.begin(), ::toupper);

        // default face order: East South West North Up Down
        GLint newObject = SOIL_load_OGL_single_cubemap (
            path.c_str(),
            orderCapitals.c_str(),
            SOIL_LOAD_AUTO,
            SOIL_CREATE_NEW_ID,
            0
        );
        if(newObject == 0) {
            throw std::runtime_error((std::string)"SOIL failed to load texture: " + SOIL_last_result());
        }
        setObject(newObject);
        setDefaultTextureOptions();
    } catch(std::exception& e) {
        throw std::runtime_error((std::string)"s3d::Texture3d::loadFromFile: " + e.what());
    }
}
void s3d::Texture3d::loadFromFiles(const std::string& xp, const std::string& xn,
                                   const std::string& yp, const std::string& yn,
                                   const std::string& zp, const std::string& zn) {
    try {
        destroy();

        std::vector<std::string> paths;
        paths.push_back(xp);
        paths.push_back(xn);
        paths.push_back(yp);
        paths.push_back(yn);
        paths.push_back(zp);
        paths.push_back(zn);
        for(unsigned int i=0;i<paths.size();i++) {
            if(!s3d::util::fileExists(paths.at(i))) {
                throw std::runtime_error(s3d::debug::failedToLoad(paths.at(i)));
            }
        }

        GLint newObject = SOIL_load_OGL_cubemap(
            xp.c_str(),
            xn.c_str(),
            yp.c_str(),
            yn.c_str(),
            zp.c_str(),
            zn.c_str(),
            SOIL_LOAD_AUTO,
            SOIL_CREATE_NEW_ID,
            0
        );
        if(newObject == 0) {
            throw std::runtime_error((std::string)"SOIL failed to load cubemap: " + SOIL_last_result());
        }
        setObject(newObject);
        setDefaultTextureOptions();
    } catch(std::exception& e) {
        throw std::runtime_error((std::string)"s3d::Cubemap::loadFromFiles: " + e.what());
    }
}

void s3d::Texture3d::destroy() {
    if(hasObject()) {
        glDeleteTextures(1, &getObject());
        setObject(0);
    }
}

void s3d::Texture3d::setWrapping(const Wrapping& wrap) {
    if(hasObject()) {
        bind();
            glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, (GLint)wrap);
            glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, (GLint)wrap);
            glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, (GLint)wrap);
            mWrapping = wrap;
        unbind();
    }
}
void s3d::Texture3d::setFiltering(const Filtering& filtering, const Mode& mode) {
    if(hasObject()) {
        bind();
            if(mode == Mode::Near || mode == Mode::Both) {
                glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, (GLint)filtering);
                mFilteringNear = filtering;
            }
            if(mode == Mode::Far || mode == Mode::Both) {
                glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, (GLint)filtering);
                mFilteringFar = filtering;
            }
        unbind();
    }
}
void s3d::Texture3d::addFilter(const Filter& filter) {
    if(hasObject()) {
        bind();
            if(filter == Filter::Anisotropic) {
                if(glewIsSupported("GL_EXT_texture_filter_anisotropic")) {
                    GLfloat max;
                    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max);
                    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_ANISOTROPY_EXT, max);
                }
            }
        unbind();
    }
}
void s3d::Texture3d::removeFilter(const Filter& filter) {
    if(hasObject()) {
        bind();
            if(filter == Filter::Anisotropic) {
                glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_ANISOTROPY_EXT, 1.0f);
            }
        unbind();
    }
}

const s3d::Texture::Wrapping& s3d::Texture3d::getWrapping() const {
    return mWrapping;
}
const s3d::Texture::Filtering& s3d::Texture3d::getFilteringNear() const {
    return mFilteringNear;
}
const s3d::Texture::Filtering& s3d::Texture3d::getFilteringFar() const {
    return mFilteringFar;
}

void s3d::Texture3d::bind(const int unit) const {
    if(hasObject()) {
        glEnable(GL_TEXTURE_CUBE_MAP);
        if((GLint)unit < getMaximumUnits()) {
            glActiveTexture(GL_TEXTURE0 + unit);
        } else {
            s3d_assert("requested texture unit outside of supported range" == 0);
        }
        glBindTexture(GL_TEXTURE_CUBE_MAP, getObject());
        glDisable(GL_TEXTURE_CUBE_MAP);
    }
}
void s3d::Texture3d::unbind() {
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    glActiveTexture(GL_TEXTURE0);
}
void s3d::Texture3d::setDefaultTextureOptions() {
    setFiltering(Filtering::Linear);
    setWrapping(Wrapping::Clamp);
}
